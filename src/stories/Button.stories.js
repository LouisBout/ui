export default {
  title: 'Example/Button',
  argTypes: {
    color: {
      control: { type: 'select' },
      options: ['primary', 'secondary', 'negative', 'warning', 'info', 'positive', 'random', 'dark', 'accent'],
    },
    onClick: {},
    size: {
      control: { type: 'select' },
      options: ['small', 'medium', 'large'],
    },
  },
}

const Template = (args) => ({
  setup() {
    return { args }
  },
  template: '<custom-button v-bind="args">Button</custom-button>',
})

export const Primary = Template.bind({})
Primary.args = {
  color: 'primary',
}

export const Secondary = Template.bind({})
Secondary.args = {
  color: 'secondary',
}

export const Large = Template.bind({})
Large.args = {
  size: 'large',
}

export const Small = Template.bind({})
Small.args = {
  size: 'small',
}

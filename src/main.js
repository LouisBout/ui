import components from './components'
import { Quasar } from 'quasar'

import '@/assets/theme/index.scss'

export default {
  install: (app, options) => {
    app.use(Quasar)
    Object.entries(components).forEach(([label, component]) => {
      app.component(label, component)
    })
  },
}

import { app } from '@storybook/vue3'
import CustomLib from '@/main.js'
app.use(CustomLib)

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
}
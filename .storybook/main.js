const path = require('path')
const vue = require('@vitejs/plugin-vue')
const { quasar, transformAssetUrls } = require('@quasar/vite-plugin')

module.exports = {
  stories: ['../src/**/*.stories.mdx', '../src/**/*.stories.@(js|jsx|ts|tsx)'],
  addons: ['@storybook/addon-links', '@storybook/addon-essentials'],
  framework: '@storybook/vue3',
  core: {
    builder: '@storybook/builder-vite',
  },
  async viteFinal(config, { configType }) {
    config.resolve.alias['@'] = path.resolve(__dirname, '../src')

    config.plugins.push(
      quasar({ sassVariables: '@/assets/theme/quasar-variables.scss' }),
    )

    return config
  },
}

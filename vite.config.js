import { fileURLToPath, URL } from 'url'

import { defineConfig } from 'vite'
import path from 'path'
import vue from '@vitejs/plugin-vue'
import { quasar, transformAssetUrls } from '@quasar/vite-plugin'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue({
      template: { transformAssetUrls },
    }),
    quasar({ sassVariables: '@/assets/theme/quasar-variables.scss' }),
  ],
  css: {
    preprocessorOptions: {
      scss: { additionalData: `@use "@/assets/theme/index.scss" as *;` },
    },
  },
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url)),
    },
  },
  build: {
    lib: {
      entry: path.join(__dirname, 'src/main.js'),
      name: 'components',
    },
    rollupOptions: {
      external: ['vue'],
      output: {
        dir: path.join(__dirname, 'dist/lib'),
        globals: {
          vue: 'Vue',
        },
      },
    },
  },
})
